### Implementação

O desafio consiste basicamente em percorrer dois arrays para identificar as informações desejadas, porém isso pode gerar um algoritmo de complexidade O(nˆm). Procurei então uma forma de evitar tal complexidade e optei por ordenar os arrays e realizar uma busca binária nos clientes (por ser a maior lista), para que pudesse chegar em um algoritmo de complexidade menor O(n log m).


### Tecnologias

Ruby 3.0.1
Docker

### Execução

# Docker
```bash
$ docker build --tag css-balancing:1.0 .
$ docker run --rm css-balancing:1.0
```

# Local
```bash
$ ruby customer_success_balancing.rb
```
