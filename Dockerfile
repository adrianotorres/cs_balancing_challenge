FROM ruby:3.0.1-buster
WORKDIR /app
COPY . .
CMD ["ruby", "customer_success_balancing.rb"]